from sys import argv
from pyedfread import edf


if __name__ == '__main__':
    print(argv[1])
    input_filename = argv[1].split(".")[0]
    samples, events, messages = edf.pread(
        f'{input_filename}.EDF',
        ignore_samples=False,
        filter=[], # ['start_trial','enemy_dead','friend_dead','participant_dead','end_trial']
        trial_marker=b'start_trial'
        # trial_marker=b'start_trial' This is originally trial_marker=b'TRIALID' however this results in an empty messages.csv
    )

    # print(f"messages.columns:{messages.columns}")
    # print(f"events.columns:{events.columns}")
    # print(f"samples.columns:{samples.columns}")
    # print(f"messages:{messages['trial']}")
    # print(f'events:{events}')

    
    # events = edf.trials2events(events, messages)
    samples.to_csv(f"{input_filename}_samples.csv")
    events.to_csv(f"{input_filename}_events.csv")
    messages.to_csv(f"{input_filename}_messages.csv")


# eyemsg_printf("RECORDED BY %s", program_name)
# python read_edf.py edfs/SUB001.edf
# https://github.com/nwilming/pyedfread


# Traceback (most recent call last):
#   File "read_edf.py", line 19, in <module>
#     events = edf.trials2events(events, messages)
#   File "C:\Users\danie\anaconda3\lib\site-packages\pyedfread\edf.py", line 79, in trials2events
#     return events.merge(messages, how='left', on=['trial'])
#   File "C:\Users\danie\anaconda3\lib\site-packages\pandas\core\frame.py", line 8195, in merge
#     return merge(
#   File "C:\Users\danie\anaconda3\lib\site-packages\pandas\core\reshape\merge.py", line 74, in merge
#     op = _MergeOperation(
#   File "C:\Users\danie\anaconda3\lib\site-packages\pandas\core\reshape\merge.py", line 668, in __init__
#     ) = self._get_merge_keys()
#   File "C:\Users\danie\anaconda3\lib\site-packages\pandas\core\reshape\merge.py", line 1033, in _get_merge_keys
#     right_keys.append(right._get_label_or_level_values(rk))
#   File "C:\Users\danie\anaconda3\lib\site-packages\pandas\core\generic.py", line 1684, in _get_label_or_level_values
#     raise KeyError(key)
# KeyError: 'trial'

